<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bettips\CodBundle\Entity\User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class User
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $login
     *
     * @ORM\Column(name="login", type="string", length=40, nullable=false)
     */
    private $login;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @var bigint $phone
     *
     * @ORM\Column(name="phone", type="bigint", nullable=false)
     */
    private $phone;

    /**
     * @var string $firstName
     *
     * @ORM\Column(name="first_name", type="string", length=100, nullable=false)
     */
    private $firstName;

    /**
     * @var string $lastName
     *
     * @ORM\Column(name="last_name", type="string", length=100, nullable=false)
     */
    private $lastName;

    /**
     * @var string $confirmationCode
     *
     * @ORM\Column(name="confirmation_code", type="string", length=20, nullable=false)
     */
    private $confirmationCode;

    /**
     * @var integer $confirmed
     *
     * @ORM\Column(name="confirmed", type="integer", nullable=false)
     */
    private $confirmed;

    /**
     * @var integer $timezoneId
     *
     * @ORM\Column(name="timezone_id", type="integer", nullable=false)
     */
    private $timezoneId;

    /**
     * @var datetime $regDate
     *
     * @ORM\Column(name="reg_date", type="datetime", nullable=false)
     */
    private $regDate;

    /**
     * @var datetime $lastOnlineDate
     *
     * @ORM\Column(name="last_online_date", type="datetime", nullable=false)
     */
    private $lastOnlineDate;

    /**
     * @var string $regIp
     *
     * @ORM\Column(name="reg_ip", type="string", length=15, nullable=false)
     */
    private $regIp;

    /**
     * @var string $lastOnlineIp
     *
     * @ORM\Column(name="last_online_ip", type="string", length=15, nullable=false)
     */
    private $lastOnlineIp;

    /**
     * @var integer $enable
     *
     * @ORM\Column(name="enable", type="integer", nullable=false)
     */
    private $enable;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * @var UserRole
     *
     * @ORM\ManyToOne(targetEntity="UserRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_role_id", referencedColumnName="id")
     * })
     */
    private $userRole;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login
     *
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * Get login
     *
     * @return string 
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param bigint $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Get phone
     *
     * @return bigint 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set confirmationCode
     *
     * @param string $confirmationCode
     */
    public function setConfirmationCode($confirmationCode)
    {
        $this->confirmationCode = $confirmationCode;
    }

    /**
     * Get confirmationCode
     *
     * @return string 
     */
    public function getConfirmationCode()
    {
        return $this->confirmationCode;
    }

    /**
     * Set confirmed
     *
     * @param integer $confirmed
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;
    }

    /**
     * Get confirmed
     *
     * @return integer 
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * Set timezoneId
     *
     * @param integer $timezoneId
     */
    public function setTimezoneId($timezoneId)
    {
        $this->timezoneId = $timezoneId;
    }

    /**
     * Get timezoneId
     *
     * @return integer 
     */
    public function getTimezoneId()
    {
        return $this->timezoneId;
    }

    /**
     * Set regDate
     *
     * @param datetime $regDate
     */
    public function setRegDate($regDate)
    {
        $this->regDate = $regDate;
    }

    /**
     * Get regDate
     *
     * @return datetime 
     */
    public function getRegDate()
    {
        return $this->regDate;
    }

    /**
     * Set lastOnlineDate
     *
     * @param datetime $lastOnlineDate
     */
    public function setLastOnlineDate($lastOnlineDate)
    {
        $this->lastOnlineDate = $lastOnlineDate;
    }

    /**
     * Get lastOnlineDate
     *
     * @return datetime 
     */
    public function getLastOnlineDate()
    {
        return $this->lastOnlineDate;
    }

    /**
     * Set regIp
     *
     * @param string $regIp
     */
    public function setRegIp($regIp)
    {
        $this->regIp = $regIp;
    }

    /**
     * Get regIp
     *
     * @return string 
     */
    public function getRegIp()
    {
        return $this->regIp;
    }

    /**
     * Set lastOnlineIp
     *
     * @param string $lastOnlineIp
     */
    public function setLastOnlineIp($lastOnlineIp)
    {
        $this->lastOnlineIp = $lastOnlineIp;
    }

    /**
     * Get lastOnlineIp
     *
     * @return string 
     */
    public function getLastOnlineIp()
    {
        return $this->lastOnlineIp;
    }

    /**
     * Set enable
     *
     * @param integer $enable
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
    }

    /**
     * Get enable
     *
     * @return integer 
     */
    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * Set region
     *
     * @param Bettips\CodBundle\Entity\Country $region
     */
    public function setRegion(\Bettips\CodBundle\Entity\Country $region)
    {
        $this->region = $region;
    }

    /**
     * Get region
     *
     * @return Bettips\CodBundle\Entity\Country 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set userRole
     *
     * @param Bettips\CodBundle\Entity\UserRole $userRole
     */
    public function setUserRole(\Bettips\CodBundle\Entity\UserRole $userRole)
    {
        $this->userRole = $userRole;
    }

    /**
     * Get userRole
     *
     * @return Bettips\CodBundle\Entity\UserRole 
     */
    public function getUserRole()
    {
        return $this->userRole;
    }
}