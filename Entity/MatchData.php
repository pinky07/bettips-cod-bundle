<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bettips\CodBundle\Entity\MatchData
 *
 * @ORM\Table(name="match_data")
 * @ORM\Entity
 */
class MatchData
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $matchOptionValue
     *
     * @ORM\Column(name="match_option_value", type="text", nullable=false)
     */
    private $matchOptionValue;

    /**
     * @var MatchOption
     *
     * @ORM\ManyToOne(targetEntity="MatchOption")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="match_option_id", referencedColumnName="id")
     * })
     */
    private $matchOption;

    /**
     * @var Match
     *
     * @ORM\ManyToOne(targetEntity="Match")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="match_id", referencedColumnName="id")
     * })
     */
    private $match;

    public function __construct(MatchOption $option)
    {
        $this->matchOption = $option;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set matchOptionValue
     *
     * @param string $matchOptionValue
     */
    public function setMatchOptionValue($matchOptionValue)
    {
        $this->matchOptionValue = $matchOptionValue;
    }

    /**
     * Get matchOptionValue
     *
     * @return string
     */
    public function getMatchOptionValue()
    {
        return $this->matchOptionValue;
    }

    /**
     * Set matchOption
     *
     * @param MatchOption $matchOption
     */
    public function setMatchOption(\Bettips\CodBundle\Entity\MatchOption $matchOption)
    {
        $this->matchOption = $matchOption;
    }

    /**
     * Get matchOption
     *
     * @return MatchOption
     */
    public function getMatchOption()
    {
        return $this->matchOption;
    }

    /**
     * Set match
     *
     * @param Match $match
     */
    public function setMatch(Match $match)
    {
        $this->match = $match;
    }

    /**
     * Get match
     *
     * @return Match
     */
    public function getMatch()
    {
        return $this->match;
    }
}