<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Bettips\CodBundle\Model\ISportable;

/**
 * Bettips\CodBundle\Entity\MatchOption
 *
 * @ORM\Table(name="match_option")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="sport_id", type="integer")
 * @ORM\DiscriminatorMap({
 *      "3"     = "Bettips\CodBundle\Entity\MatchOption\BasketballMatchOption",
 *      "18"    = "Bettips\CodBundle\Entity\MatchOption\TennisMatchOption",
 *      "19"    = "Bettips\CodBundle\Entity\MatchOption\SoccerMatchOption",
 *      "22"    = "Bettips\CodBundle\Entity\MatchOption\IceHockeyMatchOption"
 * })
 * @ORM\Entity
 */
abstract class MatchOption implements ISportable
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var Sport
     *
     * @ORM\ManyToOne(targetEntity="Sport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sport_id", referencedColumnName="id")
     * })
     */
    private $sport;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sport
     *
     * @param Sport $sport
     */
    public function setSport(Sport $sport)
    {
        $this->sport = $sport;
    }

    /**
     * Get sport
     *
     * @return Sport
     */
    public function getSport()
    {
        return $this->sport;
    }
}