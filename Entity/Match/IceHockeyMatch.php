<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 30.06.12
 * Time: 12:47
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Entity\Match;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Bettips\CodBundle\Model\Match\IIceHockeyMatch;
use Bettips\CodBundle\Entity\Sport;
use Bettips\CodBundle\Entity\Match;
use Bettips\CodBundle\Entity\Team;

/**
 * @ORM\Entity(repositoryClass="Bettips\CodBundle\Repositories\MatchRepository")
 * @ORM\Table(name="matches_ice_hockey")
 */
class IceHockeyMatch extends Match implements IIceHockeyMatch
{
    /**
     * @param \Bettips\CodBundle\Entity\Team $team
     */
    public function setHomeTeam(Team $team) {}

    /**
     * @param \Bettips\CodBundle\Entity\Team $team
     */
    public function setAwayTeam(Team $team) {}

    /**
     * @return Team
     */
    public function getHomeTeam() {}

    /**
     * @return Team
     */
    public function getAwayTeam() {}

    public function getSportId()
    {
        return self::SPORT_ID;
    }

    public function getForLivescore()
        {}
}
