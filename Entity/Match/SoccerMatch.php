<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 29.06.12
 * Time: 23:22
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Entity\Match;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Bettips\CodBundle\Model\Match\ISoccerMatch;
use Bettips\CodBundle\Model\Team\ISoccerTeam;
use Bettips\CodBundle\Entity\Team\SoccerTeam;
use Bettips\CodBundle\Entity\Match;
use Bettips\CodBundle\Entity\Sport;
use Bettips\CodBundle\Entity\Team;

/**
 * @ORM\Entity(repositoryClass="Bettips\CodBundle\Repositories\MatchRepository")
 * @ORM\Table(name="matches_soccer")
 */
class SoccerMatch extends Match implements ISoccerMatch
{
    /**
     * @ORM\Id
     * @ORM\Column(name="game_id")
     */
    protected $id;

    /**
     * @var SoccerTeam $homeTeamId
     * @ORM\ManyToOne(targetEntity="Bettips\CodBundle\Entity\Team\SoccerTeam", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="home_team_id", referencedColumnName="id")
     * })
     */
    protected $homeTeam;

    /**
     * @var SoccerTeam $awayTeamId
     * @ORM\ManyToOne(targetEntity="Bettips\CodBundle\Entity\Team\SoccerTeam", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="away_team_id", referencedColumnName="id")
     * })
     */
    protected $awayTeam;

    /**
     * @var string $matchTime
     * @ORM\Column(name="match_time", type="string", nullable=false)
     */
    protected $matchTime;

    /**
     * @var int $homeTeamHalfTimeGoals
     * @ORM\Column(name="home_team_half_time_goals", type="integer")
     */
    protected $homeTeamHalfTimeGoals;

    /**
     * @var int $awayTeamHalfTimeGoals
     * @ORM\Column(name="away_team_half_time_goals", type="integer")
     */
    protected $awayTeamHalfTimeGoals;

    /**
     * @var int $homeTeamFullTimeGoals
     * @ORM\Column(name="home_team_full_time_goals", type="integer")
     */
    protected $homeTeamFullTimeGoals;

    /**
     * @var int $awayTeamFullTimeGoals
     * @ORM\Column(name="away_team_full_time_goals", type="integer")
     */
    protected $awayTeamFullTimeGoals;

    /**
     * @var int $homeTeamYellowCards
     * @ORM\Column(name="home_team_yellow_cards", type="integer")
     */
    protected $homeTeamYellowCards;

    /**
     * @var int $awayTeamYellowCards
     * @ORM\Column(name="away_team_yellow_cards", type="integer")
     */
    protected $awayTeamYellowCards;

    /**
     * @var int $homeTeamRedCards
     * @ORM\Column(name="home_team_red_cards", type="integer")
     */
    protected $homeTeamRedCards;

    /**
     * @var int $awayTeamRedCards
     * @ORM\Column(name="away_team_red_cards", type="integer")
     */
    protected $awayTeamRedCards;

    /**
     * @var string $info
     * @ORM\Column(name="info", type="string")
     */
    protected $info;

    /**
     * @var string $info
     * @ORM\Column(name="round", type="string")
     */
    protected $round;

    /**
     * @var string $status
     * @ORM\Column(name="status", type="string")
     */
    protected $status;

    /**
     * @param \Bettips\CodBundle\Entity\Team $team
     * @throws \Exception
     */
    public function setHomeTeam(Team $team)
    {
        if(!($team instanceof ISoccerTeam))
        {
            throw new \Exception('Team interface must impements ISoccerTeam interface');
        }

        $this -> homeTeam = $team;
    }

    /**
     * @param \Bettips\CodBundle\Entity\Team $team
     * @throws \Exception
     */
    public function setAwayTeam(Team $team)
    {
        if(!($team instanceof ISoccerTeam))
        {
            throw new \Exception('Team interface must impements ISoccerTeam interface');
        }

        $this -> awayTeam = $team;
    }

    /**
     * @return SoccerTeam
     */
    public function getHomeTeam()
    {
        return $this -> homeTeam;
    }

    /**
     * @return SoccerTeam
     */
    public function getAwayTeam()
    {
        return $this -> awayTeam;
    }

    /**
     * @return int
     */
    public function getSportId()
    {
        return self::SPORT_ID;
    }

    /**
     * @param int $awayTeamFullTimeGoals
     */
    public function setAwayTeamFullTimeGoals($awayTeamFullTimeGoals)
    {
        $this->awayTeamFullTimeGoals = $awayTeamFullTimeGoals;
    }

    /**
     * @param int $awayTeamHalfTimeGoals
     */
    public function setAwayTeamHalfTimeGoals($awayTeamHalfTimeGoals)
    {
        $this->awayTeamHalfTimeGoals = $awayTeamHalfTimeGoals;
    }

    /**
     * @return int
     */
    public function getAwayTeamHalfTimeGoals()
    {
        return $this->awayTeamHalfTimeGoals;
    }

    /**
     * @param int $awayTeamRedCards
     */
    public function setAwayTeamRedCards($awayTeamRedCards)
    {
        $this->awayTeamRedCards = $awayTeamRedCards;
    }

    /**
     * @return int
     */
    public function getAwayTeamRedCards()
    {
        return $this->awayTeamRedCards;
    }

    /**
     * @param int $awayTeamYellowCards
     */
    public function setAwayTeamYellowCards($awayTeamYellowCards)
    {
        $this->awayTeamYellowCards = $awayTeamYellowCards;
    }

    /**
     * @return int
     */
    public function getAwayTeamYellowCards()
    {
        return $this->awayTeamYellowCards;
    }

    /**
     * @param int $homeTeamFullTimeGoals
     */
    public function setHomeTeamFullTimeGoals($homeTeamFullTimeGoals)
    {
        $this->homeTeamFullTimeGoals = $homeTeamFullTimeGoals;
    }

    /**
     * @return int
     */
    public function getHomeTeamFullTimeGoals()
    {
        return $this->homeTeamFullTimeGoals;
    }

    /**
     * @param int $homeTeamHalfTimeGoals
     */
    public function setHomeTeamHalfTimeGoals($homeTeamHalfTimeGoals)
    {
        $this->homeTeamHalfTimeGoals = $homeTeamHalfTimeGoals;
    }

    /**
     * @return int
     */
    public function getHomeTeamHalfTimeGoals()
    {
        return $this->homeTeamHalfTimeGoals;
    }

    /**
     * @param int $homeTeamRedCards
     */
    public function setHomeTeamRedCards($homeTeamRedCards)
    {
        $this->homeTeamRedCards = $homeTeamRedCards;
    }

    /**
     * @return int
     */
    public function getHomeTeamRedCards()
    {
        return $this->homeTeamRedCards;
    }

    /**
     * @param int $homeTeamYellowCards
     */
    public function setHomeTeamYellowCards($homeTeamYellowCards)
    {
        $this->homeTeamYellowCards = $homeTeamYellowCards;
    }

    /**
     * @return int
     */
    public function getHomeTeamYellowCards()
    {
        return $this->homeTeamYellowCards;
    }

    /**
     * @param string $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param string $matchTime
     */
    public function setMatchTime($matchTime)
    {
        $this->matchTime = $matchTime;
    }

    /**
     * @return string
     */
    public function getMatchTime()
    {
        switch($this->matchTime)
        {
            case 'FT':
                return 'завершен';
            case 'PEN':
                return 'сер. пен.';
            case 'HT':
                return 'перерыв';
        }
        return $this->matchTime;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $round
     */
    public function setRound($round)
    {
        $this->round = $round;
    }

    /**
     * @return string
     */
    public function getRound()
    {
        return $this->round;
    }

    public function getScore($delimiter = ':')
    {
        return sprintf('%s%s%s',
            $this -> getHomeTeamFullTimeGoals(),
            $delimiter,
            $this -> getAwayTeamFullTimeGoals()
        );
    }

    public function getHalfTimeScore($delimiter = ':')
    {
        return sprintf('%s%s%s',
                    $this -> getHomeTeamHalfTimeGoals(),
                    $delimiter,
                    $this -> getAwayTeamHalfTimeGoals()
                );
    }

    public function getForLivescore()
    {
        return array(
            'homeTeamGoals' => $this -> getHomeTeamFullTimeGoals(),
            'awayTeamGoals' => $this -> getAwayTeamFullTimeGoals(),
            'halfTimeScore' => $this -> getHalfTimeScore(),
            'homeTeamId'    => $this -> getHomeTeam()-> getId(),
            'awayTeamId'    => $this -> getAwayTeam()-> getId(),
            'homeTeam'      => $this -> getHomeTeam()-> getName(),
            'awayTeam'      => $this -> getAwayTeam()-> getName(),
            'competition'   => $this -> getCompetitionSeason()-> getName(),
            'matchTime'     => $this -> getMatchTime(),
            'status'        => $this -> getStatus(),
            'beginning'     => $this -> getBeginning(),
            'state'         => $this -> getState()
        );
    }

    /**
     * @return int
     */
    public function getAwayTeamFullTimeGoals()
    {
        return $this->awayTeamFullTimeGoals;
    }
}