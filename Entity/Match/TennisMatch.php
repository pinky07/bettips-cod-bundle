<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 30.06.12
 * Time: 12:44
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Entity\Match;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Bettips\CodBundle\Entity\Sport;
use Bettips\CodBundle\Model\PlayerMatch;
use Bettips\CodBundle\Entity\Player;
use Bettips\CodBundle\Model\Match\ITennisMatch;

/**
 * @ORM\Entity(repositoryClass="Bettips\CodBundle\Repositories\MatchRepository")
 * @ORM\Table(name="matches_tennis")
 */
class TennisMatch extends PlayerMatch implements ITennisMatch
{
    public function getSportId()
    {
        return self::SPORT_ID;
    }

    public function getForLivescore()
        {}
}
