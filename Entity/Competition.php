<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 02.07.12
 * Time: 21:54
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Bettips\FrameworkBundle\Model\Entity;
use Bettips\CodBundle\Model\ISportable;

/**
 * Bettips\CodBundle\Entity\CompetitionSeason
 *
 * @ORM\Table(name="competitions")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="sport_id", type="integer", fieldName = "sport")
 * @ORM\DiscriminatorMap({
 *      "3"     = "Bettips\CodBundle\Entity\Competition\BasketballCompetition",
 *      "18"    = "Bettips\CodBundle\Entity\Competition\TennisCompetition",
 *      "19"    = "Bettips\CodBundle\Entity\Competition\SoccerCompetition",
 *      "22"    = "Bettips\CodBundle\Entity\Competition\IceHockeyCompetition"
 * })
 * @ORM\Entity
 */
abstract class Competition extends Entity implements ISportable
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string $nameRu
     *
     * @ORM\Column(name="name_ru", type="string", length=300, nullable=true)
     */
    protected $nameRu;

    /**
     * @var string $nameEng
     *
     * @ORM\Column(name="name_eng", type="string", length=300, nullable=true)
     */
    protected $nameEng;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    protected $country;

    /**
     * @var CompetitionSeason
     *
     * @ORM\ManyToOne(targetEntity="CompetitionSeason")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="actual_season_id", referencedColumnName="id")
     * })
     */
    protected $actualSeason;

    /**
     * @var Sport $sport
     *
     * @ORM\ManyToOne(targetEntity="Sport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sport_id", referencedColumnName="id")
     * })
     */
    protected $sport;


    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="string", length=300, nullable=true)
     */
    protected $description;

    public function getSportId()
    {
        return $this->sport->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameRu
     *
     * @param string $nameRu
     */
    public function setNameRu($nameRu)
    {
        $this->nameRu = $nameRu;
    }

    /**
     * Get nameRu
     *
     * @return string
     */
    public function getNameRu()
    {
        return $this->nameRu;
    }

    /**
     * Set nameEng
     *
     * @param string $nameEng
     */
    public function setNameEng($nameEng)
    {
        $this->nameEng = $nameEng;
    }

    /**
     * Get nameEng
     *
     * @return string
     */
    public function getNameEng()
    {
        return $this->nameEng;
    }

    /**
     * Set country
     *
     * @param Country $country
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return Sport
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * @return CompetitionSeason
     */
    public function getActualSeason()
    {
        return $this -> actualSeason;
    }

    /**
     * @param Sport $sport
     */
    public function setSport(Sport $sport)
    {
        $this->sport = $sport;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param CompetitionSeason $actualSeason
     */
    public function setActualSeason(CompetitionSeason $actualSeason)
    {
        $this->actualSeason = $actualSeason;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (!empty($this->nameRu)?$this->getNameRu():$this->getNameEng());
    }

    public function getName()
    {
        return !empty($this->nameRu)?$this->getNameRu():$this->getNameEng();
    }
}
