<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 30.06.12
 * Time: 13:42
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Entity\Team;

use Doctrine\ORM\Mapping as ORM;
use Bettips\CodBundle\Model\Team\ISoccerTeam;
use Bettips\CodBundle\Entity\Team;
/**
 * @ORM\Entity
 */
class SoccerTeam extends Team implements ISoccerTeam
{
    /**
     * @var $homeMatches
     * @ORM\OneToMany(targetEntity="Bettips\CodBundle\Entity\Match\SoccerMatch", mappedBy="homeTeam")
     */
    protected $homeMatches;

    /**
     * @var $awayMatches
     * @ORM\OneToMany(targetEntity="Bettips\CodBundle\Entity\Match\SoccerMatch", mappedBy="awayTeam")
     */
    protected $awayMatches;

    public function getMatches()
    {

    }

    public function getSportId()
    {
        return self::SPORT_ID;
    }

    public function getHomeMatches()
    {
        return $this->homeMatches;
    }

    /**
     * @return
     */
    public function getAwayMatches()
    {
        return $this->awayMatches;
    }
}
