<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 30.06.12
 * Time: 14:08
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Entity\Team;

use Doctrine\ORM\Mapping as ORM;
use Bettips\CodBundle\Model\Sport\IBasketball;
use Bettips\CodBundle\Entity\Team;
/**
 * @ORM\Entity
 */
class BasketballTeam extends Team implements IBasketball
{
    public function getSportId()
    {
        return self::SPORT_ID;
    }
}
