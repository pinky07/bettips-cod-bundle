<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bettips\CodBundle\Entity\Region
 *
 * @ORM\Table(name="region")
 * @ORM\Entity
 */
class Region
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $nameEng
     *
     * @ORM\Column(name="name_eng", type="string", length=50, nullable=false)
     */
    private $nameEng;

    /**
     * @var string $nameRus
     *
     * @ORM\Column(name="name_rus", type="string", length=50, nullable=false)
     */
    private $nameRus;

    /**
     * @var string $code
     *
     * @ORM\Column(name="code", type="string", length=4, nullable=false)
     */
    private $code;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameEng
     *
     * @param string $nameEng
     */
    public function setNameEng($nameEng)
    {
        $this->nameEng = $nameEng;
    }

    /**
     * Get nameEng
     *
     * @return string 
     */
    public function getNameEng()
    {
        return $this->nameEng;
    }

    /**
     * Set nameRus
     *
     * @param string $nameRus
     */
    public function setNameRus($nameRus)
    {
        $this->nameRus = $nameRus;
    }

    /**
     * Get nameRus
     *
     * @return string 
     */
    public function getNameRus()
    {
        return $this->nameRus;
    }

    /**
     * Set code
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }
}