<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Bettips\FrameworkBundle\Model\Entity;
use Bettips\CodBundle\Model\ISportable;
use Bettips\CodBundle\Entity\Match\SoccerMatch;

/**
 * Базовый класс для всех типов матчей
 *
 * Bettips\CodBundle\Entity\Match
 *
 * @ORM\Table(name="matches")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="sport_id", type="integer")
 * @ORM\DiscriminatorMap({
 *      "3"     = "Bettips\CodBundle\Entity\Match\BasketballMatch",
 *      "18"    = "Bettips\CodBundle\Entity\Match\TennisMatch",
 *      "19"    = "Bettips\CodBundle\Entity\Match\SoccerMatch",
 *      "22"    = "Bettips\CodBundle\Entity\Match\IceHockeyMatch"
 * })
 * @ORM\Entity(repositoryClass="Bettips\CodBundle\Repositories\MatchRepository")
 * @ORM\HasLifecycleCallbacks
 */
abstract class Match extends Entity implements ISportable
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Дата начала матча
     * @var \DateTime $beginning
     *
     * @ORM\Column(name="beginning", type="datetime", nullable=true)
     */
    protected $beginning;

    /**
     * Внешний идентификатор матча
     * @var string $externalId
     *
     * @ORM\Column(name="external_id", type="string", length=11, nullable=true)
     */
    protected $externalId;

    /**
     * Время последнего обновления матча (timestamp)
     * @var integer $lastUpdate
     *
     * @ORM\Column(name="last_update", type="integer", nullable=false)
     */
    protected $lastUpdate;

    /**
     * Текущее состояние матча
     * 1 - завершен
     * 2 - онлайн
     * 3 - запланирован
     *
     * @var integer $status
     *
     * @ORM\Column(name="state", type="integer", nullable=true)
     */
    protected $state;

    /**
     * @var CompetitionSeason $competitionSeason
     * @ORM\ManyToOne(targetEntity="CompetitionSeason", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="competition_season_id", referencedColumnName="id")
     * })
     */
    protected $competitionSeason;

    /**
     * @var Sport $sport
     *
     * @ORM\ManyToOne(targetEntity="Sport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sport_id", referencedColumnName="id")
     * })nullable=true)
     */
    protected $sport;

    /**
     * Коллекция (Collection) данных матча
     * @var Collection $data
     *
     * @ORM\OneToMany(targetEntity="MatchData", mappedBy="match", cascade={"all"})
    */
    protected $data;

    /**
     * @var array $mappingData
     */
    protected $mappingData;

    /**
     * @var bool $needUpdate
     */
    protected $needUpdate = false;

    /**
     * Коды состояний матча
     */
    const STATE_FINISHED    = 1; // 1 - Завершен
    const STATE_IN_PLAY     = 2; // 2 - Live
    const STATE_SCHEDULED   = 3; // 3 - Запланирован


    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        if($this->needUpdate)
        {
            $this->updateFromMappingData();
        }
    }

    /**
     * Обновляет значения в объектах данных
     */
    public function updateFromMappingData()
    {
        $mappingData = $this->mappingData;

        $this->data->forAll(function($key, $matchData) use ($mappingData) {
        /**  @var MatchData $matchData */
        if(isset($mappingData[$matchData->getMatchOption()->getName()]))
        {
            $matchData->setMatchOptionValue($mappingData[$matchData->getMatchOption()->getName()]);
        }

            return true;
        });

        $this->needUpdate = false;
    }


    public function __construct()
    {
        $this->data  = new ArrayCollection();
        $this->teams = new ArrayCollection();
    }

    /**
     * Коллбэк метод, инициализирует все данные помещая их в массив $mapedData
     * @ORM\PostLoad
     */
    public function initData()
    {
        /**@var $matchData MatchData */
        foreach($this->getData() as $matchData)
        {
            $this -> mappingData[$matchData ->getMatchOption() ->getName()] = $matchData->getMatchOptionValue();
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return CompetitionSeason
     */
    public function getCompetitionSeason()
    {
        return $this -> competitionSeason;
    }

    /**
     * Set beginning
     *
     * @param \DateTime $beginning
     */
    public function setBeginning(\DateTime $beginning)
    {
        $this->beginning = $beginning;
    }

    /**
     * Get beginning
     *
     * @return \DateTime
     */
    public function getBeginning()
    {
        return $this->beginning->modify('+4 hour');
    }

    /**
     * Set externalId
     *
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * Get externalId
     *
     * @return string 
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set update
     *
     * @param integer $update
     */
    public function setUpdate($update)
    {
        $this->lastUpdate = $update;
    }

    /**
     * Get update
     *
     * @return integer 
     */
    public function getUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set status
     *
     * @param integer $stateCode
     */
    public function setState($stateCode)
    {
        $this->state = $stateCode;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->state;
    }

    /**
     * @param MatchData $data
     * @return bool
     */
    public function addData(MatchData $data)
    {
        if($data -> getMatchOption() -> getSportId() == $this-> getSportId())
        {
            // добавляем его в колекцию
            $this -> data -> add($data);
            $data -> setMatch($this);
            $this -> mappingData[$data ->getMatchOption() ->getName()] = $data->getMatchOptionValue();

            return true;
        }

        return false;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $collection
     */
    public function setData(Collection $collection)
    {
        $this->data->clear();

        foreach($collection as $element)
        {
            if($element instanceof MatchData)
                $this->addData($element);
        }
    }

    /**
     * Get data
     *
     * @return Collection
     */
    public function getData()
    {
        if($this->needUpdate)
        {
            $this->updateFromMappingData();
        }

        return $this->data;
    }

    /**
     * Возвращает параметр матча при обращении как к свойству объекта
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        if(isset($this->mappingData[$name]))
        {
            return $this->mappingData[$name];
        }

        return null;
    }

    /**
     * Устанавливает значение параметра матча
     * @param $name
     * @param $value
     * @return bool
     */
    public function __set($name, $value)
    {
        if(isset($this->mappingData[$name]))
        {
            $this -> mappingData[$name] = $value;
            $this -> needUpdate = true;
        }
    }

    public function setCompetitionSeason(CompetitionSeason $competitionSeason)
    {
        if($competitionSeason->getSportId() != $this->getSportId())
        {
            throw new \Exception('Wrong competition season sport type');
        }

        $this -> competitionSeason = $competitionSeason;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }
}