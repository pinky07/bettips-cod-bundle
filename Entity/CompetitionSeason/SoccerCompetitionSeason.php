<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 01.07.12
 * Time: 15:30
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Entity\CompetitionSeason;

use Doctrine\ORM\Mapping as ORM;
use Bettips\CodBundle\Model\Sport\ISoccer;
use Bettips\CodBundle\Entity\CompetitionSeason;
/**
 * @ORM\Entity
 */
class SoccerCompetitionSeason extends CompetitionSeason implements ISoccer
{
    public function getSportId()
    {
        return self::SPORT_ID;
    }
}
