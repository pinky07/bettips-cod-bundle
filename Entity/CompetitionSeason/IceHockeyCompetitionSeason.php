<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 01.07.12
 * Time: 15:33
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Entity\CompetitionSeason;

use Doctrine\ORM\Mapping as ORM;
use Bettips\CodBundle\Model\Sport\IIceHockey;
use Bettips\CodBundle\Entity\CompetitionSeason;

/**
 * @ORM\Entity
 */
class IceHockeyCompetitionSeason extends CompetitionSeason implements IIceHockey
{
    public function getSportId()
    {
        return self::SPORT_ID;
    }
}
