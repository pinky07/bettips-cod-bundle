<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Bettips\FrameworkBundle\Model\Entity;

/**
 * Bettips\CodBundle\Entity\Sport
 *
 * @ORM\Table(name="sport")
 * @ORM\Entity
 */
class Sport extends Entity
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $nameRu
     *
     * @ORM\Column(name="name_ru", type="string", length=200, nullable=true)
     */
    private $nameRu;

    /**
     * @var string $nameEng
     *
     * @ORM\Column(name="name_eng", type="string", length=200, nullable=true)
     */
    private $nameEng;

    /**
     * @var string $picture
     *
     * @ORM\Column(name="picture", type="string", length=300, nullable=true)
     */
    private $picture;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    private static $clasesSportPrefixes = array(
        self::BASKETBALL    => 'basketball',
        self::BASEBALL      => 'baseball',
        self::VOLLEYBALL    => 'voleyball',
        self::TENNIS        => 'tennis',
        self::SOCCER        => 'soccer'
    );

    const BASKETBALL    = 3;
    const BASEBALL      = 4;
    const VOLLEYBALL    = 9;
    const TENNIS        = 18;
    const SOCCER        = 19;
    const ICE_HOCKEY    = 22;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameRu
     *
     * @param string $nameRu
     */
    public function setNameRu($nameRu)
    {
        $this->nameRu = $nameRu;
    }

    /**
     * Get nameRu
     *
     * @return string 
     */
    public function getNameRu()
    {
        return $this->nameRu;
    }

    /**
     * Set nameEng
     *
     * @param string $nameEng
     */
    public function setNameEng($nameEng)
    {
        $this->nameEng = $nameEng;
    }

    /**
     * Get nameEng
     *
     * @return string 
     */
    public function getNameEng()
    {
        return $this->nameEng;
    }

    /**
     * Set picture
     *
     * @param string $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * Get picture
     *
     * @return string 
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @static
     * @param $sport
     * @return string
     * @throws \Exception
     */
    public static function getClassPrefixBySport($sport)
    {
        /** @var Sport $sport  */
        $sportId = ($sport instanceof Sport) ? $sport ->getId() : (int) $sport;

        if(isset(self::$clasesSportPrefixes[$sportId]))
            return ucfirst(self::$clasesSportPrefixes[$sportId]);

        throw new \Exception('Wrong sport type');
    }

    /**
     * @static
     * @return array
     */
    public static function getSports()
    {
        return array_keys(self::$clasesSportPrefixes);
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getNameRu();
    }
}