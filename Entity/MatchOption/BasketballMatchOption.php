<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 01.07.12
 * Time: 15:14
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Entity\MatchOption;

use Doctrine\ORM\Mapping as ORM;
use Bettips\CodBundle\Model\Sport\IBasketball;
use Bettips\CodBundle\Entity\MatchOption;

/**
 * @ORM\Entity
 */
class BasketballMatchOption extends MatchOption implements IBasketball
{
    public function getSportId()
    {
        return self::SPORT_ID;
    }
}
