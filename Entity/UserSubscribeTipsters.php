<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bettips\CodBundle\Entity\UserSubscribeTipsters
 *
 * @ORM\Table(name="user_subscribe_tipsters")
 * @ORM\Entity
 */
class UserSubscribeTipsters
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $userSubscriberId
     *
     * @ORM\Column(name="user_subscriber_id", type="integer", nullable=false)
     */
    private $userSubscriberId;

    /**
     * @var integer $userTipsterId
     *
     * @ORM\Column(name="user_tipster_id", type="integer", nullable=false)
     */
    private $userTipsterId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userSubscriberId
     *
     * @param integer $userSubscriberId
     */
    public function setUserSubscriberId($userSubscriberId)
    {
        $this->userSubscriberId = $userSubscriberId;
    }

    /**
     * Get userSubscriberId
     *
     * @return integer 
     */
    public function getUserSubscriberId()
    {
        return $this->userSubscriberId;
    }

    /**
     * Set userTipsterId
     *
     * @param integer $userTipsterId
     */
    public function setUserTipsterId($userTipsterId)
    {
        $this->userTipsterId = $userTipsterId;
    }

    /**
     * Get userTipsterId
     *
     * @return integer 
     */
    public function getUserTipsterId()
    {
        return $this->userTipsterId;
    }
}