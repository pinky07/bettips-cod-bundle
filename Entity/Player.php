<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Bettips\CodBundle\Model\ISportable;

/**
 * Bettips\CodBundle\Entity\Player
 *
 * @ORM\Table(name="player")
 * @ORM\DiscriminatorMap({
 *      "18"    = "Bettips\CodBundle\Entity\Player\TennisPlayer"
 * })
 * @ORM\Entity
 */
abstract class Player implements ISportable
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $nameRu
     *
     * @ORM\Column(name="name_ru", type="string", length=100, nullable=false)
     */
    private $nameRu;

    /**
     * @var string $nameEng
     *
     * @ORM\Column(name="name_eng", type="string", length=100, nullable=false)
     */
    private $nameEng;

    /**
     * @var integer $countryId
     *
     * @ORM\Column(name="country_id", type="integer", nullable=false)
     */
    private $countryId;

    /**
     * @var integer $sportId
     *
     * @ORM\Column(name="sport_id", type="integer", nullable=false)
     */
    private $sportId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameRu
     *
     * @param string $nameRu
     */
    public function setNameRu($nameRu)
    {
        $this->nameRu = $nameRu;
    }

    /**
     * Get nameRu
     *
     * @return string 
     */
    public function getNameRu()
    {
        return $this->nameRu;
    }

    /**
     * Set nameEng
     *
     * @param string $nameEng
     */
    public function setNameEng($nameEng)
    {
        $this->nameEng = $nameEng;
    }

    /**
     * Get nameEng
     *
     * @return string 
     */
    public function getNameEng()
    {
        return $this->nameEng;
    }

    /**
     * Set countryId
     *
     * @param integer $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }

    /**
     * Get countryId
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->countryId;
    }
}