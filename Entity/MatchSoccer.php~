<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bettips\CodBundle\Entity\MatchSoccer
 *
 * @ORM\Table(name="match_soccer")
 * @ORM\Entity
 */
class MatchSoccer
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $externalId
     *
     * @ORM\Column(name="external_id", type="string", length=30, nullable=false)
     */
    private $externalId;

    /**
     * @var datetime $kickoff
     *
     * @ORM\Column(name="kickoff", type="datetime", nullable=false)
     */
    private $kickoff;

    /**
     * @var string $team1
     *
     * @ORM\Column(name="team_1", type="string", length=100, nullable=false)
     */
    private $team1;

    /**
     * @var string $team2
     *
     * @ORM\Column(name="team_2", type="string", length=100, nullable=false)
     */
    private $team2;

    /**
     * @var integer $team1HalfTimeGoals
     *
     * @ORM\Column(name="team_1_half_time_goals", type="integer", nullable=false)
     */
    private $team1HalfTimeGoals;

    /**
     * @var integer $team2HalfTimeGoals
     *
     * @ORM\Column(name="team_2_half_time_goals", type="integer", nullable=false)
     */
    private $team2HalfTimeGoals;

    /**
     * @var integer $team1FullTimeGoals
     *
     * @ORM\Column(name="team_1_full_time_goals", type="integer", nullable=false)
     */
    private $team1FullTimeGoals;

    /**
     * @var integer $team2FullTimeGoals
     *
     * @ORM\Column(name="team_2_full_time_goals", type="integer", nullable=false)
     */
    private $team2FullTimeGoals;

    /**
     * @var integer $team1Yellowcards
     *
     * @ORM\Column(name="team_1_yellowcards", type="integer", nullable=false)
     */
    private $team1Yellowcards;

    /**
     * @var integer $team2Yellowcards
     *
     * @ORM\Column(name="team_2_yellowcards", type="integer", nullable=false)
     */
    private $team2Yellowcards;

    /**
     * @var integer $team1Redcards
     *
     * @ORM\Column(name="team_1_redcards", type="integer", nullable=false)
     */
    private $team1Redcards;

    /**
     * @var integer $team2Redcards
     *
     * @ORM\Column(name="team_2_redcards", type="integer", nullable=false)
     */
    private $team2Redcards;

    /**
     * @var string $halfTimeScore
     *
     * @ORM\Column(name="half_time_score", type="string", length=100, nullable=false)
     */
    private $halfTimeScore;

    /**
     * @var string $fullTimeScore
     *
     * @ORM\Column(name="full_time_score", type="string", length=100, nullable=false)
     */
    private $fullTimeScore;

    /**
     * @var string $status
     *
     * @ORM\Column(name="status", type="string", length=300, nullable=true)
     */
    private $status;

    /**
     * @var string $state
     *
     * @ORM\Column(name="state", type="string", length=300, nullable=true)
     */
    private $state;

    /**
     * @var string $comptition
     *
     * @ORM\Column(name="comptition", type="string", length=200, nullable=false)
     */
    private $comptition;

    /**
     * @var string $region
     *
     * @ORM\Column(name="region", type="string", length=100, nullable=false)
     */
    private $region;

    /**
     * @var string $season
     *
     * @ORM\Column(name="season", type="string", length=20, nullable=false)
     */
    private $season;

    /**
     * @var string $round
     *
     * @ORM\Column(name="round", type="string", length=200, nullable=false)
     */
    private $round;

    /**
     * @var text $info
     *
     * @ORM\Column(name="info", type="text", nullable=false)
     */
    private $info;

    /**
     * @var text $currentEvent
     *
     * @ORM\Column(name="current_event", type="text", nullable=false)
     */
    private $currentEvent;

    /**
     * @var Team
     *
     * @ORM\ManyToOne(targetEntity="Team")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="team_1_external_id", referencedColumnName="external_id")
     * })
     */
    private $team1External;

    /**
     * @var Team
     *
     * @ORM\ManyToOne(targetEntity="Team")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="team_2_external_id", referencedColumnName="external_id")
     * })
     */
    private $team2External;

    /**
     * @var Competition
     *
     * @ORM\ManyToOne(targetEntity="Competition")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="competition_external_id", referencedColumnName="external_id")
     * })
     */
    private $competitionExternal;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set externalId
     *
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * Get externalId
     *
     * @return string 
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set kickoff
     *
     * @param datetime $kickoff
     */
    public function setKickoff($kickoff)
    {
        $this->kickoff = $kickoff;
    }

    /**
     * Get kickoff
     *
     * @return datetime 
     */
    public function getKickoff()
    {
        return $this->kickoff;
    }

    /**
     * Set team1
     *
     * @param string $team1
     */
    public function setTeam1($team1)
    {
        $this->team1 = $team1;
    }

    /**
     * Get team1
     *
     * @return string 
     */
    public function getTeam1()
    {
        return $this->team1;
    }

    /**
     * Set team2
     *
     * @param string $team2
     */
    public function setTeam2($team2)
    {
        $this->team2 = $team2;
    }

    /**
     * Get team2
     *
     * @return string 
     */
    public function getTeam2()
    {
        return $this->team2;
    }

    /**
     * Set team1HalfTimeGoals
     *
     * @param integer $team1HalfTimeGoals
     */
    public function setTeam1HalfTimeGoals($team1HalfTimeGoals)
    {
        $this->team1HalfTimeGoals = $team1HalfTimeGoals;
    }

    /**
     * Get team1HalfTimeGoals
     *
     * @return integer 
     */
    public function getTeam1HalfTimeGoals()
    {
        return $this->team1HalfTimeGoals;
    }

    /**
     * Set team2HalfTimeGoals
     *
     * @param integer $team2HalfTimeGoals
     */
    public function setTeam2HalfTimeGoals($team2HalfTimeGoals)
    {
        $this->team2HalfTimeGoals = $team2HalfTimeGoals;
    }

    /**
     * Get team2HalfTimeGoals
     *
     * @return integer 
     */
    public function getTeam2HalfTimeGoals()
    {
        return $this->team2HalfTimeGoals;
    }

    /**
     * Set team1FullTimeGoals
     *
     * @param integer $team1FullTimeGoals
     */
    public function setTeam1FullTimeGoals($team1FullTimeGoals)
    {
        $this->team1FullTimeGoals = $team1FullTimeGoals;
    }

    /**
     * Get team1FullTimeGoals
     *
     * @return integer 
     */
    public function getTeam1FullTimeGoals()
    {
        return $this->team1FullTimeGoals;
    }

    /**
     * Set team2FullTimeGoals
     *
     * @param integer $team2FullTimeGoals
     */
    public function setTeam2FullTimeGoals($team2FullTimeGoals)
    {
        $this->team2FullTimeGoals = $team2FullTimeGoals;
    }

    /**
     * Get team2FullTimeGoals
     *
     * @return integer 
     */
    public function getTeam2FullTimeGoals()
    {
        return $this->team2FullTimeGoals;
    }

    /**
     * Set team1Yellowcards
     *
     * @param integer $team1Yellowcards
     */
    public function setTeam1Yellowcards($team1Yellowcards)
    {
        $this->team1Yellowcards = $team1Yellowcards;
    }

    /**
     * Get team1Yellowcards
     *
     * @return integer 
     */
    public function getTeam1Yellowcards()
    {
        return $this->team1Yellowcards;
    }

    /**
     * Set team2Yellowcards
     *
     * @param integer $team2Yellowcards
     */
    public function setTeam2Yellowcards($team2Yellowcards)
    {
        $this->team2Yellowcards = $team2Yellowcards;
    }

    /**
     * Get team2Yellowcards
     *
     * @return integer 
     */
    public function getTeam2Yellowcards()
    {
        return $this->team2Yellowcards;
    }

    /**
     * Set team1Redcards
     *
     * @param integer $team1Redcards
     */
    public function setTeam1Redcards($team1Redcards)
    {
        $this->team1Redcards = $team1Redcards;
    }

    /**
     * Get team1Redcards
     *
     * @return integer 
     */
    public function getTeam1Redcards()
    {
        return $this->team1Redcards;
    }

    /**
     * Set team2Redcards
     *
     * @param integer $team2Redcards
     */
    public function setTeam2Redcards($team2Redcards)
    {
        $this->team2Redcards = $team2Redcards;
    }

    /**
     * Get team2Redcards
     *
     * @return integer 
     */
    public function getTeam2Redcards()
    {
        return $this->team2Redcards;
    }

    /**
     * Set halfTimeScore
     *
     * @param string $halfTimeScore
     */
    public function setHalfTimeScore($halfTimeScore)
    {
        $this->halfTimeScore = $halfTimeScore;
    }

    /**
     * Get halfTimeScore
     *
     * @return string 
     */
    public function getHalfTimeScore()
    {
        return $this->halfTimeScore;
    }

    /**
     * Set fullTimeScore
     *
     * @param string $fullTimeScore
     */
    public function setFullTimeScore($fullTimeScore)
    {
        $this->fullTimeScore = $fullTimeScore;
    }

    /**
     * Get fullTimeScore
     *
     * @return string 
     */
    public function getFullTimeScore()
    {
        return $this->fullTimeScore;
    }

    /**
     * Set status
     *
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set state
     *
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set comptition
     *
     * @param string $comptition
     */
    public function setComptition($comptition)
    {
        $this->comptition = $comptition;
    }

    /**
     * Get comptition
     *
     * @return string 
     */
    public function getComptition()
    {
        return $this->comptition;
    }

    /**
     * Set region
     *
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set season
     *
     * @param string $season
     */
    public function setSeason($season)
    {
        $this->season = $season;
    }

    /**
     * Get season
     *
     * @return string 
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * Set round
     *
     * @param string $round
     */
    public function setRound($round)
    {
        $this->round = $round;
    }

    /**
     * Get round
     *
     * @return string 
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set info
     *
     * @param text $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * Get info
     *
     * @return text 
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set currentEvent
     *
     * @param text $currentEvent
     */
    public function setCurrentEvent($currentEvent)
    {
        $this->currentEvent = $currentEvent;
    }

    /**
     * Get currentEvent
     *
     * @return text 
     */
    public function getCurrentEvent()
    {
        return $this->currentEvent;
    }

    /**
     * Set team1External
     *
     * @param Bettips\CodBundle\Entity\Team $team1External
     */
    public function setTeam1External(\Bettips\CodBundle\Entity\Team $team1External)
    {
        $this->team1External = $team1External;
    }

    /**
     * Get team1External
     *
     * @return Bettips\CodBundle\Entity\Team 
     */
    public function getTeam1External()
    {
        return $this->team1External;
    }

    /**
     * Set team2External
     *
     * @param Bettips\CodBundle\Entity\Team $team2External
     */
    public function setTeam2External(\Bettips\CodBundle\Entity\Team $team2External)
    {
        $this->team2External = $team2External;
    }

    /**
     * Get team2External
     *
     * @return Bettips\CodBundle\Entity\Team 
     */
    public function getTeam2External()
    {
        return $this->team2External;
    }

    /**
     * Set competitionExternal
     *
     * @param Bettips\CodBundle\Entity\Competition $competitionExternal
     */
    public function setCompetitionExternal(\Bettips\CodBundle\Entity\Competition $competitionExternal)
    {
        $this->competitionExternal = $competitionExternal;
    }

    /**
     * Get competitionExternal
     *
     * @return Bettips\CodBundle\Entity\Competition 
     */
    public function getCompetitionExternal()
    {
        return $this->competitionExternal;
    }
}