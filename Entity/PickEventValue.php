<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bettips\CodBundle\Entity\PickEventValue
 *
 * @ORM\Table(name="pick_event_value")
 * @ORM\Entity
 */
class PickEventValue
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $pickEventStyleId
     *
     * @ORM\Column(name="pick_event_style_id", type="integer", nullable=false)
     */
    private $pickEventStyleId;

    /**
     * @var string $value
     *
     * @ORM\Column(name="value", type="string", length=30, nullable=false)
     */
    private $value;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pickEventStyleId
     *
     * @param integer $pickEventStyleId
     */
    public function setPickEventStyleId($pickEventStyleId)
    {
        $this->pickEventStyleId = $pickEventStyleId;
    }

    /**
     * Get pickEventStyleId
     *
     * @return integer 
     */
    public function getPickEventStyleId()
    {
        return $this->pickEventStyleId;
    }

    /**
     * Set value
     *
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
}