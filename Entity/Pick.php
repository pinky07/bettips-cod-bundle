<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bettips\CodBundle\Entity\Pick
 *
 * @ORM\Table(name="pick")
 * @ORM\Entity
 */
class Pick
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $pickTypeId
     *
     * @ORM\Column(name="pick_type_id", type="integer", nullable=false)
     */
    private $pickTypeId;

    /**
     * @var integer $bookieId
     *
     * @ORM\Column(name="bookie_id", type="integer", nullable=false)
     */
    private $bookieId;

    /**
     * @var datetime $createDate
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var integer $userId
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var string $userIp
     *
     * @ORM\Column(name="user_ip", type="string", length=12, nullable=false)
     */
    private $userIp;

    /**
     * @var decimal $stake
     *
     * @ORM\Column(name="stake", type="decimal", nullable=false)
     */
    private $stake;

    /**
     * @var decimal $totalOdds
     *
     * @ORM\Column(name="total_odds", type="decimal", nullable=false)
     */
    private $totalOdds;

    /**
     * @var decimal $profit
     *
     * @ORM\Column(name="profit", type="decimal", nullable=false)
     */
    private $profit;

    /**
     * @var integer $pickResultId
     *
     * @ORM\Column(name="pick_result_id", type="integer", nullable=false)
     */
    private $pickResultId;

    /**
     * @var text $comment
     *
     * @ORM\Column(name="comment", type="text", nullable=false)
     */
    private $comment;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var integer $hit
     *
     * @ORM\Column(name="hit", type="integer", nullable=false)
     */
    private $hit;

    /**
     * @var datetime $update
     *
     * @ORM\Column(name="update", type="datetime", nullable=false)
     */
    private $update;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pickTypeId
     *
     * @param integer $pickTypeId
     */
    public function setPickTypeId($pickTypeId)
    {
        $this->pickTypeId = $pickTypeId;
    }

    /**
     * Get pickTypeId
     *
     * @return integer 
     */
    public function getPickTypeId()
    {
        return $this->pickTypeId;
    }

    /**
     * Set bookieId
     *
     * @param integer $bookieId
     */
    public function setBookieId($bookieId)
    {
        $this->bookieId = $bookieId;
    }

    /**
     * Get bookieId
     *
     * @return integer 
     */
    public function getBookieId()
    {
        return $this->bookieId;
    }

    /**
     * Set createDate
     *
     * @param datetime $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * Get createDate
     *
     * @return datetime 
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set userIp
     *
     * @param string $userIp
     */
    public function setUserIp($userIp)
    {
        $this->userIp = $userIp;
    }

    /**
     * Get userIp
     *
     * @return string 
     */
    public function getUserIp()
    {
        return $this->userIp;
    }

    /**
     * Set stake
     *
     * @param decimal $stake
     */
    public function setStake($stake)
    {
        $this->stake = $stake;
    }

    /**
     * Get stake
     *
     * @return decimal 
     */
    public function getStake()
    {
        return $this->stake;
    }

    /**
     * Set totalOdds
     *
     * @param decimal $totalOdds
     */
    public function setTotalOdds($totalOdds)
    {
        $this->totalOdds = $totalOdds;
    }

    /**
     * Get totalOdds
     *
     * @return decimal 
     */
    public function getTotalOdds()
    {
        return $this->totalOdds;
    }

    /**
     * Set profit
     *
     * @param decimal $profit
     */
    public function setProfit($profit)
    {
        $this->profit = $profit;
    }

    /**
     * Get profit
     *
     * @return decimal 
     */
    public function getProfit()
    {
        return $this->profit;
    }

    /**
     * Set pickResultId
     *
     * @param integer $pickResultId
     */
    public function setPickResultId($pickResultId)
    {
        $this->pickResultId = $pickResultId;
    }

    /**
     * Get pickResultId
     *
     * @return integer 
     */
    public function getPickResultId()
    {
        return $this->pickResultId;
    }

    /**
     * Set comment
     *
     * @param text $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get comment
     *
     * @return text 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set status
     *
     * @param integer $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set hit
     *
     * @param integer $hit
     */
    public function setHit($hit)
    {
        $this->hit = $hit;
    }

    /**
     * Get hit
     *
     * @return integer 
     */
    public function getHit()
    {
        return $this->hit;
    }

    /**
     * Set update
     *
     * @param datetime $update
     */
    public function setUpdate($update)
    {
        $this->update = $update;
    }

    /**
     * Get update
     *
     * @return datetime 
     */
    public function getUpdate()
    {
        return $this->update;
    }
}