<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Bettips\FrameworkBundle\Model\Entity;

/**
 * Bettips\CodBundle\Entity\Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity(repositoryClass="Bettips\CodBundle\Repositories\CountryRepository")
 */
class Country extends Entity
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $nameRu
     *
     * @ORM\Column(name="name_ru", type="string", length=200, nullable=false)
     */
    private $nameRu;

    /**
     * @var string $nameEng
     *
     * @ORM\Column(name="name_eng", type="string", length=200, nullable=false)
     */
    private $nameEng;

    /**
     * @var integer $areaTypeId
     *
     * @ORM\Column(name="area_type_id", type="integer", nullable=false)
     */
    private $areaTypeId;

    /**
     * @var string $code
     *
     * @ORM\Column(name="code", type="string", length=3, nullable=true)
     */
    private $code;

    /**
     * @var string $iso3
     *
     * @ORM\Column(name="iso_3", type="string", length=3, nullable=true)
     */
    private $iso3;

    /**
     * @var string $iso2
     *
     * @ORM\Column(name="iso_2", type="string", length=2, nullable=true)
     */
    private $iso2;

    /**
     * @var smallint $numeric
     *
     * @ORM\Column(name="numeric", type="smallint", nullable=true)
     */
    private $numeric;

    /**
     * @var string $flag
     *
     * @ORM\Column(name="flag", type="string", length=300, nullable=false)
     */
    private $flag;

    /**
     * @var Collection $competitions
     * @ORM\OneToMany(targetEntity="Competition", mappedBy="country")
     */
    protected $competitions;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameRu
     *
     * @param string $nameRu
     */
    public function setNameRu($nameRu)
    {
        $this->nameRu = $nameRu;
    }

    /**
     * Get nameRu
     *
     * @return string 
     */
    public function getNameRu()
    {
        return $this->nameRu;
    }

    /**
     * Set nameEng
     *
     * @param string $nameEng
     */
    public function setNameEng($nameEng)
    {
        $this->nameEng = $nameEng;
    }

    /**
     * Get nameEng
     *
     * @return string 
     */
    public function getNameEng()
    {
        return $this->nameEng;
    }

    /**
     * Set areaTypeId
     *
     * @param integer $areaTypeId
     */
    public function setAreaTypeId($areaTypeId)
    {
        $this->areaTypeId = $areaTypeId;
    }

    /**
     * Get areaTypeId
     *
     * @return integer 
     */
    public function getAreaTypeId()
    {
        return $this->areaTypeId;
    }

    /**
     * Set code
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set iso3
     *
     * @param string $iso3
     */
    public function setIso3($iso3)
    {
        $this->iso3 = $iso3;
    }

    /**
     * Get iso3
     *
     * @return string 
     */
    public function getIso3()
    {
        return $this->iso3;
    }

    /**
     * Set iso2
     *
     * @param string $iso2
     */
    public function setIso2($iso2)
    {
        $this->iso2 = $iso2;
    }

    /**
     * Get iso2
     *
     * @return string 
     */
    public function getIso2()
    {
        return $this->iso2;
    }

    /**
     * Set numeric
     *
     * @param smallint $numeric
     */
    public function setNumeric($numeric)
    {
        $this->numeric = $numeric;
    }

    /**
     * Get numeric
     *
     * @return smallint 
     */
    public function getNumeric()
    {
        return $this->numeric;
    }

    /**
     * Set flag
     *
     * @param string $flag
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;
    }

    /**
     * Get flag
     *
     * @return string 
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getNameRu();
    }

    public function getCompetitions()
    {
        return $this -> competitions;
    }

    public function getName()
    {
        return !empty($this->nameRu)?$this->getNameRu():$this->getNameEng();
    }
}