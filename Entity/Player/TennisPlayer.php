<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 01.07.12
 * Time: 15:18
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Entity\Player;

use Bettips\CodBundle\Entity\Player;
use Bettips\CodBundle\Model\Sport\ITennis;

/**
 * @ORM\Entity
 */
class TennisPlayer extends Player implements ITennis
{
    public function getSportId()
    {
        return self::SPORT_ID;
    }
}
