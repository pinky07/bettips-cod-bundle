<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Bettips\FrameworkBundle\Model\Entity;
use Bettips\CodBundle\Model\ISportable;

/**
 * Bettips\CodBundle\Entity\CompetitionSeason
 *
 * @ORM\Table(name="competition_seasons")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="sport_id", type="integer")
 * @ORM\DiscriminatorMap({
 *      "3"     = "Bettips\CodBundle\Entity\CompetitionSeason\BasketballCompetitionSeason",
 *      "18"    = "Bettips\CodBundle\Entity\CompetitionSeason\TennisCompetitionSeason",
 *      "19"    = "Bettips\CodBundle\Entity\CompetitionSeason\SoccerCompetitionSeason",
 *      "22"    = "Bettips\CodBundle\Entity\CompetitionSeason\IceHockeyCompetitionSeason"
 * })
 * @ORM\Entity
 */
abstract class CompetitionSeason extends Entity implements ISportable
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string $nameRu
     *
     * @ORM\Column(name="name_ru", type="string", length=300, nullable=true)
     */
    protected $nameRu;

    /**
     * @var string $nameEng
     *
     * @ORM\Column(name="name_eng", type="string", length=300, nullable=true)
     */
    protected $nameEng;

    /**
     * @var \DateTime $startDate
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    protected $startDate;

    /**
     * @var \DateTime $endDate
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    protected $endDate;

    /**
     * @var string $code
     *
     * @ORM\Column(name="code", type="string", length=6, nullable=true)
     */
    protected $code;

    /**
     * @var integer $externalId
     *
     * @ORM\Column(name="external_id", type="integer", nullable=true)
     */
    protected $externalId;

    /**
     * @var integer $countryId
     *
     * @ORM\Column(name="country_id", type="integer", nullable=false)
     */
    protected $countryId;

    /**
     * @var Competition $competition
     * @ORM\ManyToOne(targetEntity="Competition", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="competition_id", referencedColumnName="id")
     * })
     */
    protected $competition;

    /**
     * @var string $regionCode
     *
     * @ORM\Column(name="region_code", type="string", length=5, nullable=true)
     */
    private $regionCode;

    /**
     * @var string $region
     *
     * @ORM\Column(name="region", type="string", length=300, nullable=true)
     */
    private $region;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameRu
     *
     * @param string $nameRu
     */
    public function setNameRu($nameRu)
    {
        $this->nameRu = $nameRu;
    }

    /**
     * Get nameRu
     *
     * @return string 
     */
    public function getNameRu()
    {
        return $this->nameRu;
    }

    /**
     * Set nameEng
     *
     * @param string $nameEng
     */
    public function setNameEng($nameEng)
    {
        $this->nameEng = $nameEng;
    }

    /**
     * Get nameEng
     *
     * @return string 
     */
    public function getNameEng()
    {
        return $this->nameEng;
    }

    /**
     * Set code
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set externalId
     *
     * @param integer $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * Get externalId
     *
     * @return integer 
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set countryId
     *
     * @param integer $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }

    /**
     * Get countryId
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set regionCode
     *
     * @param string $regionCode
     */
    public function setRegionCode($regionCode)
    {
        $this->regionCode = $regionCode;
    }

    /**
     * Get regionCode
     *
     * @return string 
     */
    public function getRegionCode()
    {
        return $this->regionCode;
    }

    /**
     * Set region
     *
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getNameEng();
    }

    public function getName()
    {
        return !empty($this->nameRu)?$this->getNameRu():$this->getNameEng();
    }

    /**
     * @param \Bettips\CodBundle\Entity\Competition $competition
     */
    public function setCompetition($competition)
    {
        $this->competition = $competition;
    }

    /**
     * @return \Bettips\CodBundle\Entity\Competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }
}