<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bettips\CodBundle\Entity\UserSubscribeMatches
 *
 * @ORM\Table(name="user_subscribe_matches")
 * @ORM\Entity
 */
class UserSubscribeMatches
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $userId
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var integer $matchId
     *
     * @ORM\Column(name="match_id", type="integer", nullable=false)
     */
    private $matchId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set matchId
     *
     * @param integer $matchId
     */
    public function setMatchId($matchId)
    {
        $this->matchId = $matchId;
    }

    /**
     * Get matchId
     *
     * @return integer 
     */
    public function getMatchId()
    {
        return $this->matchId;
    }
}