<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bettips\CodBundle\Entity\PickEvent
 *
 * @ORM\Table(name="pick_event")
 * @ORM\Entity
 */
class PickEvent
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $pickId
     *
     * @ORM\Column(name="pick_id", type="integer", nullable=false)
     */
    private $pickId;

    /**
     * @var integer $matchId
     *
     * @ORM\Column(name="match_id", type="integer", nullable=false)
     */
    private $matchId;

    /**
     * @var integer $pickStyleId
     *
     * @ORM\Column(name="pick_style_id", type="integer", nullable=false)
     */
    private $pickStyleId;

    /**
     * @var integer $pickEventValue
     *
     * @ORM\Column(name="pick_event_value", type="integer", nullable=false)
     */
    private $pickEventValue;

    /**
     * @var decimal $odds
     *
     * @ORM\Column(name="odds", type="decimal", nullable=false)
     */
    private $odds;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pickId
     *
     * @param integer $pickId
     */
    public function setPickId($pickId)
    {
        $this->pickId = $pickId;
    }

    /**
     * Get pickId
     *
     * @return integer 
     */
    public function getPickId()
    {
        return $this->pickId;
    }

    /**
     * Set matchId
     *
     * @param integer $matchId
     */
    public function setMatchId($matchId)
    {
        $this->matchId = $matchId;
    }

    /**
     * Get matchId
     *
     * @return integer 
     */
    public function getMatchId()
    {
        return $this->matchId;
    }

    /**
     * Set pickStyleId
     *
     * @param integer $pickStyleId
     */
    public function setPickStyleId($pickStyleId)
    {
        $this->pickStyleId = $pickStyleId;
    }

    /**
     * Get pickStyleId
     *
     * @return integer 
     */
    public function getPickStyleId()
    {
        return $this->pickStyleId;
    }

    /**
     * Set pickEventValue
     *
     * @param integer $pickEventValue
     */
    public function setPickEventValue($pickEventValue)
    {
        $this->pickEventValue = $pickEventValue;
    }

    /**
     * Get pickEventValue
     *
     * @return integer 
     */
    public function getPickEventValue()
    {
        return $this->pickEventValue;
    }

    /**
     * Set odds
     *
     * @param decimal $odds
     */
    public function setOdds($odds)
    {
        $this->odds = $odds;
    }

    /**
     * Get odds
     *
     * @return decimal 
     */
    public function getOdds()
    {
        return $this->odds;
    }
}