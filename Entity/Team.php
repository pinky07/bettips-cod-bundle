<?php

namespace Bettips\CodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Bettips\FrameworkBundle\Model\Entity;
use Bettips\CodBundle\Model\ISportable;
use Bettips\CodBundle\Entity\Competition;
use Bettips\CodBundle\Entity\Country;

/**
 * Bettips\CodBundle\Entity\Team
 *
 * @ORM\Table(name="team")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="sport_id", type="integer")
 * @ORM\DiscriminatorMap({
 *      "3"     = "Bettips\CodBundle\Entity\Team\BasketballTeam",
 *      "19"    = "Bettips\CodBundle\Entity\Team\SoccerTeam",
 *      "22"    = "Bettips\CodBundle\Entity\Team\IceHockeyTeam"
 * })
 * @ORM\Entity
 */
abstract class Team extends Entity implements ISportable
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $nameRu
     *
     * @ORM\Column(name="name_ru", type="string", length=300, nullable=true)
     */
    private $nameRu;

    /**
     * @var string $nameEng
     *
     * @ORM\Column(name="name_eng", type="string", length=300, nullable=true)
     */
    private $nameEng;

    /**
     * @var string $leagueCode
     *
     * @ORM\Column(name="league_code", type="string", length=3, nullable=true)
     */
    private $leagueCode;

    /**
     * @var integer $externalId
     *
     * @ORM\Column(name="external_id", type="integer", nullable=true)
     */
    private $externalId;

    /**
     * @var string $countryCode
     *
     * @ORM\Column(name="country_code", type="string", length=3, nullable=true)
     */
    private $countryCode;

    /**
     * @var text $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string $logo
     *
     * @ORM\Column(name="logo", type="string", length=300, nullable=true)
     */
    private $logo;

    /**
     * @var Competition
     *
     * @ORM\ManyToOne(targetEntity="Competition")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="league_id", referencedColumnName="id")
     * })
     */
    private $league;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var Sport $sport
     *
     * @ORM\ManyToOne(targetEntity="Sport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sport_id", referencedColumnName="id")
     * })nullable=true)
     */
    protected $sport;

    /**
     * @param  $sportId
     */
    protected $sportId;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this -> id;
    }

    /**
     * Set nameRu
     *
     * @param string $nameRu
     */
    public function setNameRu($nameRu)
    {
        $this -> nameRu = $nameRu;
    }

    /**
     * Get nameRu
     *
     * @return string 
     */
    public function getNameRu()
    {
        return $this->nameRu;
    }

    /**
     * Set nameEng
     *
     * @param string $nameEng
     */
    public function setNameEng($nameEng)
    {
        $this->nameEng = $nameEng;
    }

    /**
     * Get nameEng
     *
     * @return string 
     */
    public function getNameEng()
    {
        return $this->nameEng;
    }

    /**
     * Set leagueCode
     *
     * @param string $leagueCode
     */
    public function setLeagueCode($leagueCode)
    {
        $this->leagueCode = $leagueCode;
    }

    /**
     * Get leagueCode
     *
     * @return string 
     */
    public function getLeagueCode()
    {
        return $this->leagueCode;
    }

    /**
     * Set externalId
     *
     * @param integer $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * Get externalId
     *
     * @return integer 
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * Get countryCode
     *
     * @return string 
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set logo
     *
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }


    /**
     * Set league
     *
     * @param Bettips\CodBundle\Entity\Competition $league
     */
    public function setLeague(Competition $league)
    {
        $this->league = $league;
    }

    /**
     * Get league
     *
     * @return Bettips\CodBundle\Entity\Competition 
     */
    public function getLeague()
    {
        return $this->league;
    }

    public function getSport()
    {
        return $this->sport;
    }

    /**
     * Set country
     *
     * @param Country $country
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    public function getName()
    {
        return !empty($this->nameRu)?$this->getNameRu():$this->getNameEng();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (!empty($this->nameRu)?$this->getNameRu():$this->getNameEng());
    }
}