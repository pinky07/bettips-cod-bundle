<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 02.07.12
 * Time: 22:16
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Entity\Competition;

use Doctrine\ORM\Mapping as ORM;
use Bettips\CodBundle\Model\Sport\ITennis;
use Bettips\CodBundle\Entity\Competition;

/**
 * @ORM\Entity
 */
class TennisCompetition extends Competition implements ITennis
{
    public function getSportId()
    {
        return self::SPORT_ID;
    }
}
