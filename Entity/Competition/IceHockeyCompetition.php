<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 02.07.12
 * Time: 22:17
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Entity\Competition;

use Doctrine\ORM\Mapping as ORM;
use Bettips\CodBundle\Model\Sport\IIceHockey;
use Bettips\CodBundle\Entity\Competition;

/**
 * @ORM\Entity
 */
class IceHockeyCompetition extends Competition implements IIceHockey
{
    public function getSportId()
    {
        return self::SPORT_ID;
    }
}
