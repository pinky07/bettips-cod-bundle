<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 22.04.12
 * Time: 12:04
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Classes\Parsers\LiveScore;

use Bettips\CodBundle\Classes\Parsers\LiveScore;

class Soccer extends LiveScore
{
    protected $path     = 'soccer';
    protected $sportId  = \Bettips\CodBundle\Entity\Sport::SOCCER;

    public function getPath()
    {
        return $this->path;
    }

    public function process($data)
    {
        $splits     = explode('^^~~@@', $data);
        $matches    = explode("\n", $splits[3]);

        foreach($matches as $match)
        {
            if(empty($match))
            {
                continue;
            }

            $match_details = explode('##', $match);

            $this->matchesData[$match_details[0]] = array();
        }
        var_dump($this->matchesData); exit;
    }
}
