<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 22.04.12
 * Time: 11:46
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Classes\Parsers;

interface IParseble
{
    public function parse();
}
