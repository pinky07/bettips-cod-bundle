<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 22.04.12
 * Time: 0:43
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Classes\Parsers;

use Bettips\CodBundle\Classes\Parsers;

abstract class LiveScore implements IParseble
{
    protected $path         = 'ajax.php';
    protected $date         = null;
    protected $sportId;
    protected $sportOptions;
    protected $matchesData  = array();
    protected $paramsCompare= array();

    const LiveScoreHost = 'www.scorespro.com';

    public function parse()
    {
        $data = file_get_contents($this->getParseUrl());
        $this -> process($data);
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getParseUrl()
    {
        return sprintf('http://%s/%s/ajax.php%s',
            self::LiveScoreHost,
            $this->getPath(),
            ($this->date != null)?'?date='.$this->date:''
        );
    }

    /**
     * @static
     * @param $name
     * @return LiveScore
     */
    public static function createParser($name)
    {
        $class_name = sprintf('Bettips\CodBundle\Classes\Parsers\LiveScore\%s', ucfirst($name));
        if(!class_exists($class_name))
        {
           return false;
        }

        //new \Doctrine\ORM\EntityManager()

        $parser = new $class_name;

        if($parser instanceof LiveScore)
        {
            return $parser;
        }

        return false;
    }

    abstract public function getPath();

    abstract public function process($data);
}
