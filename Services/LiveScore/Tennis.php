<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 27.06.12
 * Time: 11:21
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Services\LiveScore;
use Bettips\CodBundle\Services\LiveScore;
use Bettips\CodBundle\Entity\Sport;

class Tennis extends LiveScore
{
    /**
     * @var int
     */
    protected $sportId = Sport::TENNIS;

    /**
     * @var string
     */
    protected $parseUri= 'tennis/ajax.php';

    protected function updateFromResponse($response)
    {
        $splits     = explode('^^~~@@', $response); # $splits[3] - количество матчей в лайфе
        $matchLines = explode("\n", $splits[2]);

        //var_dump($splits); exit;
        foreach($matchLines as $line)
        {
            if(empty($line))
            {
                continue;
            }

            // Переобразуем параметры в необходимый вид
            $matchDetails = explode('##', $line);


            var_dump($matchDetails);
        }

        //var_dump($matchLines); exit;

        //echo $response;
        exit;
    }

    protected function getFormattedData($match_details)
    {
        $scorePattern = '/.*?(\d{1,2})\s*-\s*(\d{1,2})/';

        return array(
                'info'                      => $match_details[16],
                'external_id'               => $match_details[0],
                'competition_external_id'   => $match_details[1],
                'status'                    => $match_details[3],
                'state'                     => $match_details[4],
                'home'
            );
    }
}
