<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 27.06.12
 * Time: 11:21
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Services\LiveScore;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Bettips\CodBundle\Services\LiveScore;
use Bettips\CodBundle\Entity\Sport;
use Bettips\CodBundle\Entity\Match;
use Bettips\CodBundle\Entity\Team;
use Bettips\CodBundle\Entity\MatchData;
use Bettips\CodBundle\Entity\MatchOption;
use Bettips\CodBundle\EntityManager\MatchManager;
use Bettips\CodBundle\EntityManager\TeamManager;
use Bettips\CodBundle\Entity\CompetitionSeason;

class Soccer extends LiveScore
{
    /**
     * @var int
     */
    protected $sportId = Sport::SOCCER;

    /**
     * @var string
     */
    protected $parseUri= 'soccer/ajax.php';

    protected function updateFromResponse($response)
    {
        $splits     = explode('^^~~@@', $response);
        $matchLines = explode("\n", $splits[3]);

        //Массив со всеми деталями всех матчей
        $allMatchesData         = array();
        
        //Массив внешних ключей команд
        $teamsExternalId        = array();
        
        //Массив внешних ключей турниров
        $competitionExternalId  = array();

        foreach($matchLines as $line)
        {
            if(empty($line))
            {
                continue;
            }

            // Переобразуем параметры в необходимый вид
            $matchDetails = $this->getFormattedData(explode('##', $line));

            $allMatchesData[$matchDetails['externalId']] = $matchDetails;

            array_push($teamsExternalId, $matchDetails['homeTeamExternalId'], $matchDetails['awayTeamExternalId']);
            array_push($competitionExternalId, $matchDetails['competitionExternalId']);
        }

        if(empty($allMatchesData))
        {
            return null;
        }


        // Массив внешних идишников команд
        $teamsExternalId        = array_unique($teamsExternalId);
        // Массив внешних идишников турниров
        $competitionExternalId  = array_unique($competitionExternalId);
        // Массив внешних идишников матчей
        $matchesExternalid      = array_keys($allMatchesData);

        // Заранее получаем все команды, матчи и турниры, 
        // с текущими external_id, что бы не обращаться к бд, 
        // при каждой итерации цикла foreach
        
        /** @var Collection $teams  */
        $teams          = new ArrayCollection($this -> teamManager ->findBy(
            array('externalId'  => $teamsExternalId)
        ));

        /** @var Collection $matches  */
        $matches        = new ArrayCollection($this -> matchManager -> findBy(array(
            'externalId' => $matchesExternalid)
        ));

        /** @var Collection $competitions  */
        $competitions   = new ArrayCollection($this -> competitionSeasonManager -> findBy(array(
            'externalId' => $competitionExternalId)
        ));

        foreach($allMatchesData as $currentMatchData)
        {

            //Ищем домашнюю команду в коллекции, если не нашли создаем новую
            /** @var $homeTeam Team */
            $homeTeam = $this -> teamManager -> findOrCreateTeamFromCollection(
                $teams,
                $currentMatchData['homeTeamExternalId'],
                array(
                    'nameEng' => $currentMatchData['homeTeamName']
                )
            );

            //Если создали новую команду добавляем её в коллекцию
            if(!$homeTeam->getId())
            {
                $teams ->add($homeTeam);
            }
            //Ищем гостевую команду в коллекции, если не нашли создаем новую команду
            /** @var $awayTeam Team */
            $awayTeam = $this -> teamManager -> findOrCreateTeamFromCollection(
                $teams,
                $currentMatchData['awayTeamExternalId'],
                array(
                    'nameEng' => $currentMatchData['awayTeamName']
                )
            );
            //Если новая команда добавляем в коллекцию
            if(!$awayTeam->getId())
            {
                $teams ->add($awayTeam);
            }

            /** @var CompetitionSeason $competitionSeason */
            $competitionSeason = $this -> competitionSeasonManager -> findOrCreateFromCollection(
                $competitions,
                $currentMatchData['competitionExternalId'],
                array(
                    'nameEng' => $currentMatchData['comptitionName'],
                    'region'  => $currentMatchData['region'],
                )
            );

            if(!$competitionSeason->getId())
            {
                $competitions ->add($competitionSeason);
            }

            //Переменная - объект текущего матча
            $match = $this -> matchManager -> findOrCreateFromCollection($matches, $currentMatchData['externalId']);

            //Обновляем параметры матча
            $match -> setAttributes(array_merge(
                $currentMatchData,
                array(
                    'competitionSeason' => $competitionSeason,
                    'homeTeam'          => $homeTeam,
                    'awayTeam'          => $awayTeam,
                    'beginning'         => new \DateTime($currentMatchData['kickoff'])
                )
            ));

            $match -> setUpdate(time());

            //Сохраняем матч
            $this
                -> em
                -> persist($match);
        }
        //Отправляем в бд
        $this -> em -> flush();
        $this -> em -> clear();
        return true;
    }

    protected function getFormattedData($match_details)
    {
        $scorePattern = '/.*?(\d{1,2})\s*-\s*(\d{1,2})/';

        $homeTeamHalfTimeGoals = function() use($scorePattern, $match_details) {
            preg_match($scorePattern, $match_details['16'], $match);

            return isset($match[1])?(int)$match[1]:'';
        };
        $awayTeamHalfTimeGoals = function() use($scorePattern, $match_details) {
            preg_match($scorePattern, $match_details['16'], $match);

            return isset($match[2])?(int)$match[2]:'';
        };
        $homeTeamFullTimeGoals = function() use($scorePattern, $match_details) {
            preg_match($scorePattern, $match_details['15'], $match);

            return isset($match[1])?(int)$match[1]:'';

        };
        $awayTeamFullTimeGoals = function() use($scorePattern, $match_details) {
            preg_match($scorePattern, $match_details['15'], $match);

            return isset($match[2])?(int)$match[2]:'';
        };

        return array(
                'homeTeamName'          => $match_details[10],
                'awayTeamName'          => $match_details[11],
                'homeTeamHalfTimeGoals' => $homeTeamHalfTimeGoals(),
                'awayTeamHalfTimeGoals' => $awayTeamHalfTimeGoals(),
                'homeTeamFullTimeGoals' => $homeTeamFullTimeGoals(),
                'awayTeamFullTimeGoals' => $awayTeamFullTimeGoals(),
                'homeTeamYellowCards'   => isset($match_details['20']) ? (substr_count($match_details['20'], 'yellowcard')+(substr_count($match_details['20'], 'second_yellowcard')*2)) : 'n/a',
                'awayTeamYellowCards'   => isset($match_details['21']) ? (substr_count($match_details['21'], 'yellowcard')+(substr_count($match_details['21'], 'second_yellowcard')*2)) : 'n/a',
                'homeTeamRedCards'      => isset($match_details['20']) ? substr_count($match_details['20'], 'redcard') : 'n/a',
                'awayTeamRedCards'      => isset($match_details['21']) ? substr_count($match_details['21'], 'redcard') : 'n/a',
                'halfTimeScore'         => $match_details[16],
                'fullTimeScore'         => $match_details[15],
                'status'                => $match_details[17],
                'state'                 => isset($match_details[22]) ? $match_details[22] : 1,
                'homeTeamExternalId'    => $match_details[6],
                'awayTeamExternalId'    => $match_details[7],
                'comptitionName'        => $match_details[8],
                'competitionExternalId' => $match_details[1],
                'region'                => isset($match_details[27]) ? $match_details[27] :'',
                'season'                => isset($match_details[31]) ? $match_details[31] :'',
                'round'                 => $match_details[5],
                'comments'              => isset($match_details[25]) ? $match_details[25] :'',
                'externalId'            => $match_details[0],
                'info'                  => isset($match_details[24]) ? $match_details[24] :'',
                'matchTime'             => strip_tags($match_details[4]),
                'kickoff'               => $match_details[3]
            );
    }
}