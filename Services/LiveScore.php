<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 22.04.12
 * Time: 21:19
 * Базовый класс для лайфскор сервисов,
 * любой лайфскор сервис должен наследовать этот класс
 */
namespace Bettips\CodBundle\Services;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Bettips\CodBundle\Entity\Sport;
use Bettips\CodBundle\EntityManager\MatchManager;
use Bettips\CodBundle\EntityManager\TeamManager;
use Bettips\CodBundle\EntityManager\CompetitionSeasonManager;

abstract class LiveScore
{
    /**
     * Идентификатор вида спорта
     * @var $sportId
     */
    protected $sportId;

    /**
     * @var $parseUri
     */
    protected $parseUri;

    /**
     * @var array
     */
    protected $matches;

    /**
     * Опции матча
     * @var array $matchOptions
     */
    protected $matchOptions;

    /**
     * @var EntityManager $em
     */
    protected $em;

    /**
     * @var LoaderClient
     */
    protected $loaderClient;

    /**
     * @var LoaderClient
     */
    protected $teamManager;

    /**
     *
     */
    protected $matchManager;

    /**
     *
     */
    protected $competitionSeasonManager;

    /**
     *
     */
    const HOST = 'www.scorespro.com';

    /**
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param LoaderClient $loaderClient
     */
    public function __construct(EntityManager $entityManager, LoaderClient $loaderClient)
    {
        $this -> em                 = $entityManager;

        $this -> em ->getConnection()->getConfiguration()->setSQLLogger(null);

        $this -> matchManager       = new MatchManager(
            $this->em, $this->sportId);
        $this -> competitionSeasonManager = new CompetitionSeasonManager(
            $this->em, $this->sportId);
        $this -> teamManager        = new TeamManager(
            $this->em, $this->sportId);
        $this -> loaderClient       = $loaderClient;
    }

    /**
     * Обновляет данные о матчах в бд
     * @param null $date
     * @return bool
     */
    public function updateMatches($date = null)
    {
        /*$this->matchOptions = $this -> em
                                    -> getRepository('BettipsCodBundle:MatchOption')
                                    -> findBy(array('sport' => $this->sportId));*/

        if($date == null)
        {
            $requestParams = array(
                'g_sort'=> 'live'
            );
        }
        else
        {
            $requestParams = array(
                'date'  => date('Y-m-d', strtotime($date))
            );
        }

        $requestUri = sprintf(
                    'http://%s/%s',
                    self::HOST,
                    $this->parseUri
        );

        // Создаем запрос
        $request = Request::create($requestUri, 'GET', $requestParams);

        // Отправляем запрос
        if(!$this->loaderClient->send($request))
        {
            return false;
        }

        $this->updateFromResponse($this->loaderClient->getResponse());

        return true;
    }

    /**
     * @abstract
     * @param $response
     */
    abstract protected function updateFromResponse($response);
}
