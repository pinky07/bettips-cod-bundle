<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 30.06.12
 * Time: 11:38
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Model;

interface IPlayerable
{
    public function getPlayers();

    public function setPlayers();
}
