<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 08.07.12
 * Time: 11:08
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Model\Match;
use Bettips\CodBundle\Model\Sport\ITennis;
use Bettips\CodBundle\Model\IPlayerable;
use Bettips\CodBundle\Model\ILivescorable;

interface ITennisMatch extends ITennis, IPlayerable, ILivescorable {}
