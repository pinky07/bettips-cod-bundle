<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 08.07.12
 * Time: 11:03
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Model\Match;
use Bettips\CodBundle\Model\Sport\IBasketball;
use Bettips\CodBundle\Model\ITeamable;
use Bettips\CodBundle\Model\ILivescorable;

interface IBasketballMatch extends IBasketball, ITeamable, ILivescorable {}
