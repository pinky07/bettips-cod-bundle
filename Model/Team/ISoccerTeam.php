<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 08.07.12
 * Time: 11:34
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Model\Team;
use Bettips\CodBundle\Model\Sport\IBasketball;
use Bettips\CodBundle\Model\IMatchable;

interface ISoccerTeam extends IBasketball, IMatchable
{}
