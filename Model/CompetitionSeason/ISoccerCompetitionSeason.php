<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 08.07.12
 * Time: 11:46
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Model\Match;
use Bettips\CodBundle\Model\Sport\ISoccer;
use Bettips\CodBundle\Model\IMatchable;

interface ISoccerCompetitionSeason extends ISoccer, IMatchable
{}
