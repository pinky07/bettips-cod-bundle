<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 08.07.12
 * Time: 11:36
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Model;

interface IMatchable
{
    public function getMatches();
}
