<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 30.06.12
 * Time: 11:40
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Model;

use Doctrine\Common\Collections\Collection;
use Bettips\CodBundle\Entity\Team;

interface ITeamable
{
    /**
     * @abstract
     * @param \Bettips\CodBundle\Entity\Team $team
     */
    public function setHomeTeam(Team $team);

    /**
     * @abstract
     * @param \Bettips\CodBundle\Entity\Team $team
     */
    public function setAwayTeam(Team $team);

    /**
     * @abstract
     * @return \Bettips\CodBundle\Entity\Team
     */
    public function getHomeTeam();

    /**
     * @abstract
     * @return Team
     */
    public function getAwayTeam();
}
