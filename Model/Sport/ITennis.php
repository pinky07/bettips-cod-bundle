<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 01.07.12
 * Time: 14:59
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Model\Sport;

use Bettips\CodBundle\Entity\Sport;
use Bettips\CodBundle\Model\ISportable;

interface ITennis extends ISportable
{
    const SPORT_ID = Sport::TENNIS;
}
