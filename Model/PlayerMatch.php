<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 29.06.12
 * Time: 23:54
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Bettips\CodBundle\Entity\Match;
use Bettips\CodBundle\Entity\Player;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
abstract class PlayerMatch extends Match implements IPlayerable
{
    public function setPlayers() {}

    public function getPlayers() {}

    public function getHomePlayer() {}

    public function getAwayPlayer() {}
}
