<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 10.07.12
 * Time: 1:34
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Model;

interface ILivescorable
{
    public function getForLivescore();
}
