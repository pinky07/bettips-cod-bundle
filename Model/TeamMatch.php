<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 29.06.12
 * Time: 23:54
 * Родительский класс для всех матчей командных видов спорта
 */
namespace Bettips\CodBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Bettips\CodBundle\Entity\Match;
use Bettips\CodBundle\Entity\Team;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
abstract class TeamMatch extends Match implements ITeamable
{
    /**
     * Коллекция комменд связанных с матчем
     * @var Collection $teams
     * 
     * @ORM\ManyToMany(targetEntity="Bettips\CodBundle\Entity\Team", inversedBy="matches", cascade={"all"})
     * @ORM\JoinTable(name="team_matches", joinColumns={
     *    @ORM\JoinColumn(name="match_id", referencedColumnName="id")
     * })
     */
    protected $teams;

    /**
     * Добавляет новую команду к матчу
     * @param \Bettips\CodBundle\Entity\Team $team
     */
    public function addTeam(Team $team)
    {
        // Проверяем, существует ли в коллекции эта команда
        if(!($this->teams->exists(function($key, $element) use ($team) {
            return ($element == $team);
        })))
        {
            // Eсли её там нет, то добавляем
            $this->teams->add($team);
        }
    }

    /**
     * Устанавливает коллекцию команд
     * @param \Doctrine\Common\Collections\Collection $collection
     */
    public function setTeams(Collection $collection)
    {
        $this->teams = new ArrayCollection();

        foreach($collection as $element)
        {
            if(!($element instanceof Team))
                continue;

            $this->teams->add($element);
        }
    }

    /**
     * Возвращает коллекцию комманд
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeams()
    {
        return $this -> teams;
    }
}
