<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 01.07.12
 * Time: 1:52
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Model;

interface ISportable
{
    /**
     * @abstract
     * @return int
     */
    public function getSportId();
}
