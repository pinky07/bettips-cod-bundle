<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 05.07.12
 * Time: 23:06
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\EntityManager;

use Doctrine\ORM\EntityManager;
use Bettips\FrameworkBundle\EntityManager\BaseEntityManager;
use Bettips\CodBundle\Entity\Sport;
use Doctrine\Common\Collections\Collection;
use Bettips\CodBundle\Entity\CompetitionSeason;

class CompetitionManager extends BaseEntityManager
{
    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param $sport
     */
    public function __construct(EntityManager $em, $sport = null)
    {
        if(null == $sport)
        {
            $class = 'BettipsCodBundle:Competition';
        } else
        {
            $class = sprintf('BettipsCodBundle:Competition\%sCompetition',
                        Sport::getClassPrefixBySport($sport)
                    );
        }

        parent::__construct($em, $class);
    }
}
