<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 30.06.12
 * Time: 13:32
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\EntityManager;

use Doctrine\ORM\EntityManager;
use Bettips\FrameworkBundle\EntityManager\BaseEntityManager;
use Bettips\CodBundle\Entity\Team;
use Bettips\CodBundle\Entity\Sport;
use Doctrine\Common\Collections\Collection;

class TeamManager extends BaseEntityManager
{
    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param $sport
     */
    public function __construct(EntityManager $em, $sport)
    {
        $class = sprintf('BettipsCodBundle:Team\%sTeam',
            Sport::getClassPrefixBySport($sport)
        );

        parent::__construct($em, $class);
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $teamsCollection
     * @param $externalId
     * @param array $params
     * @return \Bettips\CodBundle\Entity\Team|\Bettips\FrameworkBundle\Model\Entity
     */
    public function findOrCreateTeamFromCollection(Collection $teamsCollection, $externalId, array $params)
    {
        /** @var Team $team */
        foreach($teamsCollection as $team)
        {
            if($team->getExternalId() == $externalId)
            {
                return $team;
            }
        }

        $params['externalId'] = $externalId;

        $team = $this -> create($params);

        return $team;
    }
}
