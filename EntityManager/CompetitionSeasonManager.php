<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 30.06.12
 * Time: 14:39
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\EntityManager;
use Doctrine\ORM\EntityManager;
use Bettips\FrameworkBundle\EntityManager\BaseEntityManager;
use Bettips\CodBundle\Entity\Sport;
use Doctrine\Common\Collections\Collection;
use Bettips\CodBundle\Entity\CompetitionSeason;

class CompetitionSeasonManager extends BaseEntityManager
{
    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param $sport
     */
    public function __construct(EntityManager $em, $sport)
    {
        $class = sprintf('BettipsCodBundle:CompetitionSeason\%sCompetitionSeason',
            Sport::getClassPrefixBySport($sport)
        );

        parent::__construct($em, $class);
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $competitionSesonCollection
     * @param $externalId
     * @param array $params
     * @return CompetitionSeason
     */
    public function findOrCreateFromCollection(Collection $competitionSesonCollection, $externalId, array $params)
    {
        /** @var CompetitionSeason $competitionSeson */
        foreach($competitionSesonCollection as $competitionSeson)
        {
            if($competitionSeson->getExternalId() == $externalId)
            {
                return $competitionSeson;
            }
        }

        $params['externalId'] = $externalId;

        $competitionSeson = $this -> create($params);

        return $competitionSeson;
    }
}
