<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 30.06.12
 * Time: 11:56
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\Collection;
use Bettips\FrameworkBundle\EntityManager\BaseEntityManager;
use Bettips\CodBundle\Entity\Sport;
use Bettips\CodBundle\Entity\Match;

class MatchManager extends BaseEntityManager
{
    /** @var \Bettips\CodBundle\Repositories\MatchRepository $repository */
    protected $repository;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param $sport
     */
    public function __construct(EntityManager $em, $sport = null)
    {
        if(null == $sport)
        {
            $class = 'BettipsCodBundle:Match';
        } else
        {
            $class = sprintf('BettipsCodBundle:Match\%sMatch',
                        Sport::getClassPrefixBySport($sport)
                    );
        }

        parent::__construct($em, $class);
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $matchesCollection
     * @param $externalId
     * @return \Bettips\CodBundle\Entity\Match
     */
    public function findOrCreateFromCollection(Collection $matchesCollection, $externalId)
    {
        /** @var Match $match */
        foreach($matchesCollection as $match)
        {
            if($match->getExternalId() == $externalId)
            {
                return $match;
            }
        }

        $team = $this -> create(array('externalId' => $externalId));

        return $team;
    }

    public function findByRequest(Request $request, array $orderBy = null, $limit = null, $offset = null)
    {
        $params = $this->getFindParamsFromRequest($request);

        return $this->findByData($params, $orderBy, $limit);
    }

    public function findByData(array $params, array $orderBy = null, $limit = null)
    {
        return $this->repository->findByData($params, $orderBy, $limit);
    }
}
