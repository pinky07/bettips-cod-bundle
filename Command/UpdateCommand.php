<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 22.04.12
 * Time: 21:28
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\Output;
use Bettips\CodBundle\Console\LiveScore;

class UpdateCommand extends ContainerAwareCommand
{
    protected $availibleSportParams = array('soccer');

    public function configure()
    {
        $this
            ->setName('livescore:update')
            ->setDescription('livescore loader')
            ->addArgument('sport', InputArgument::REQUIRED, 'loded sport service identification')
            ->addArgument('date', InputArgument::OPTIONAL, 'start parse date');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        if(!in_array($input->getArgument('sport'), $this->availibleSportParams))
        {
            $result = 'wrong sport param';
        }
        else
        {
            // Путь pid файла
            $pidFile = sprintf('%s/tmp/%s_livescore.pid',
                $this->getContainer()->get('kernel')->getRootDir(),
                $input->getArgument('sport')
            );

            if(file_exists($pidFile))
            {
                exit("already running\n");
            }

            file_put_contents($pidFile, '');

            $livescoreService = $this
                                      -> getContainer()
                                      -> get(sprintf('%s_livescore', $input->getArgument('sport')));

            while(file_exists($pidFile))
            {
                $livescoreService->updateMatches(date('Y-m-d'));
                
                sleep(40);

                if(memory_get_usage() > 80000000)
                {
                    unlink($pidFile); exit;
                }
            }

            $result = 'complete';
        }


        $output->writeln($result);
    }
}
