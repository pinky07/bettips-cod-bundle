<?php

namespace Bettips\CodBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    
    public function indexAction($name)
    {
        return $this->render('BettipsCodBundle:Default:index.html.twig', array('name' => $name));
    }
}
