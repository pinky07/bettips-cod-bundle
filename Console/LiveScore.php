<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 23.06.12
 * Time: 22:05
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Console;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Bettips\CodBundle\Command;
use Symfony\Component\HttpKernel\KernelInterface;

class LiveScore extends Application
{
    public function __construct(KernelInterface $kernel) {
            parent::__construct('Welcome to livescore app', '1.0');

            $this->addCommands(array(
                new Command\Update(),
            ));
        }
}
