<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 08.07.12
 * Time: 20:09
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Repositories;
use Doctrine\ORM\EntityRepository;

class CountryRepository extends EntityRepository
{
    public function findWithCompetitions($sport)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb -> select('country')
            -> from('Bettips\CodBundle\Entity\Country', 'country')
            -> join('country.competitions', 'competitions')
            -> where('competitions.sport = :sport')
            -> setParameter('sport', $sport);

        return $qb->getQuery()->getResult();
    }
}
