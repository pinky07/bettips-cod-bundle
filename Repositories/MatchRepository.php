<?php
/**
 * Created by JetBrains PhpStorm.
 * User: pinky
 * Date: 25.06.12
 * Time: 22:49
 * To change this template use File | Settings | File Templates.
 */
namespace Bettips\CodBundle\Repositories;

use Doctrine\ORM\EntityRepository;
use \Doctrine\Common\Collections\ArrayCollection;
use Bettips\CodBundle\Entity\Sport;

class MatchRepository extends EntityRepository
{
    /**
     * Поиск матча по параметрам матча
     * (названия команд, название турнира, стадия турнира, дата итд..)
     * @param int $sportCode -  код вида спорта
     * @param array $data - массив параметров матча
     * @return array
     */
    /*public function findByData($sportCode, array $data)
    {
        $classPrefix = Sport::getClassPrefixBySport($sportCode);

        $className   = sprintf('Bettips\CodBundle\Entity\Match\%sMatch', $classPrefix);

        $qb = $this->_em->createQueryBuilder();

        $qb -> select('m')
            -> from($className, 'm')
            -> join('m.data', 'md')
            -> join('md.matchOption', 'mo');

        foreach($data as $option => $value)
        {
            $qb->orWhere(
                $qb->expr()->andX(
                       $qb->expr()->eq('mo.name', "'{$option}'"),
                       (is_array($value)) ?
                           $qb->expr()->in('md.matchOptionValue', $value):
                           $qb->expr()->eq('md.matchOptionValue', "'{$value}'")
            ));
        }

        return $qb->getQuery()->getResult();
    }*/

    /**
     * @param array $params
     * @param array|null $orderBy
     * @param null $limit
     * @return array
     * @throws \Exception
     */
    public function findByData(array $params, array $orderBy = null, $limit = null)
    {
        $qb = $this->_em->createQueryBuilder();

        $qb -> select('m')
            -> from('Bettips\CodBundle\Entity\Match', 'm');

        foreach($params as $param => $value)
        {
            if(isset($this->_em->getClassMetadata('Bettips\CodBundle\Entity\Match')->reflFields[$param]))
            {
                if($param == 'beginning')
                {
                    $date = new \DateTime($value);
                    $date->modify('+4 hour');

                    $dateFrom   = $date->format('Y-m-d 00:00:00');
                    $dateTo     = $date->format('Y-m-d 23:59:59');

                    $qb -> andWhere("m.beginning >= :dateFrom")
                        -> setParameter('dateFrom', new \DateTime($dateFrom))
                        -> andWhere("m.beginning <= :dateTo")
                        -> setParameter('dateTo', new \DateTime($dateTo));
                } else
                {
                    $qb -> andWhere("m.{$param} = :{$param}")
                        -> setParameter($param, $value);
                }
            }
        }

        return $qb->getQuery()->getResult();
    }
}
